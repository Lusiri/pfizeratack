using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            /* Detecci�n 1 dedo
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    ProcesarToque(Input.GetTouch(0).position);
                }
            }*/

            /*Detecci�n multit�ctil*/
            Touch[] toques = Input.touches;
            foreach (Touch toque in toques)
            {
                ProcesarToque(toque.position);
            }
            /*Fin Detecci�n multit�ctil*/

        }
        else if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ProcesarToque(Input.mousePosition);
            }
        }

    }

    private void ProcesarToque(Vector2 posicion)
    {
        //Procesar qu� hacer cuando el usuario toque la pantalla

        Vector3 nuevoVector = Camera.main.ScreenToWorldPoint(posicion);
        Vector2 posicionToque = new Vector2(nuevoVector.x, nuevoVector.y);
        Collider2D hit = Physics2D.OverlapPoint(posicionToque);

        if (hit && hit.transform.tag == "Respawn")
        {

            Destroy(hit.transform.gameObject);

        }


    }









    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);


    }


    public void QuitGame()
    {
        Application.Quit();


    }






}