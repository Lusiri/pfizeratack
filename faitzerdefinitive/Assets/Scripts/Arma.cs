using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{

    public AudioClip SonidoDisparo;
    AudioSource auidoS;

    public GameObject bullet;
    public Transform spawnPoint;

    public float shotForce = 1500;
    public float shotRate = 0.5f;

    private float shotRateTime = 0;


    // Start is called before the first frame update
    void Start()
    {
        auidoS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
       



    }

    public void Disparar()
    {
        if (Time.time > shotRateTime)
        {
            GameObject newBullet;

            newBullet = Instantiate(bullet, spawnPoint.position, spawnPoint.rotation);

            newBullet.GetComponent<Rigidbody>().AddForce(spawnPoint.forward * shotForce);

            shotRateTime = Time.time + shotRate;

            Destroy(newBullet, 2);

            auidoS.PlayOneShot(SonidoDisparo);
        }


    }













}
