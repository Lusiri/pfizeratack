using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemigo : MonoBehaviour
{

    private Rigidbody rbEnemigo;
    public GameObject  jugador;
    public float speed = 5.0f;
   
    // Start is called before the first frame update
    void Start()
    {
        rbEnemigo = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, jugador.transform.position, step);
        //rbEnemigo.AddForce(jugador.transform.position - transform.position);

      
    }


    private void OnTriggerEnter(Collider other)
    {

        
        if (other.gameObject.CompareTag("Jugador"))
        {
            
            Destroy(this.gameObject);

        }

    }







}
