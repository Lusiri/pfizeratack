using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Jugador : MonoBehaviour
{
    public float vidaActual = 5.0f;
    public GameObject BalasPrefabs;
    public Text vida;
    public float puntos;
    public Text puntuacion;




    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.CompareTag("Enemigo"))
        {
            vidaActual--;
            vida.text = vidaActual.ToString();
            puntos++;
            puntuacion.text = puntos.ToString();
        }

    }
}
