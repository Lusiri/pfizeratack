
using UnityEngine;

public class VidaEnemigo : MonoBehaviour
{
  public float health = 50f;


  public void TakeDamage (float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
    }
    
    void Die()
    {
        ScoreScript.scoreValue += 10;
        Destroy(this.gameObject);
    }
}
