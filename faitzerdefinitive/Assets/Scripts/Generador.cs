using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Generador : MonoBehaviour
{
    public GameObject VirusPrefabs;
    public GameObject jugador;
    public float timeGenerador = 4f, RangoCreacion = 50f;
    private float LastShoot;
    public float vidaJugador;
    public bool generacion = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!generacion) return;

        vidaJugador = jugador.GetComponent<Jugador>().vidaActual;
        

        if(vidaJugador < 0)
        {
            Debug.Log("hola");
            generacion = false;
            
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);


            
        }
        if (Time.time > LastShoot + 1f)
        {
            Invoke("Generando", 0.0f);
            LastShoot = Time.time;
            
        }
     
        //jugador.GetComponent<Jugador>().vidaActual;
    }

    public void Generando()
    {
        Vector3 SpawnPosicion = new Vector3(0, 0, 0);
        SpawnPosicion = this.transform.position + Random.onUnitSphere * RangoCreacion;
        SpawnPosicion = new Vector3(SpawnPosicion.x, -16, SpawnPosicion.z);


        GameObject virus = Instantiate(VirusPrefabs, SpawnPosicion, Quaternion.identity);
    }
}
